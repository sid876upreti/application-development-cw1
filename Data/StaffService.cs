﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Wed2.Data
{
    public static class StaffService
    {
        private static void SaveAll(List<StaffRecord> staff)
        {
            string appDataDirectoryPath = Utils.GetAppDirectoryPath();
            string staffFilePath = Utils.GetStaffFilePath();

            if (!Directory.Exists(appDataDirectoryPath))
            {
                Directory.CreateDirectory(appDataDirectoryPath);
            }

            var json = JsonSerializer.Serialize(staff);
            File.WriteAllText(staffFilePath, json);
        }

        public static List<StaffRecord> GetAll()
        {
            string todosFilePath = Utils.GetStaffFilePath();
            if (!File.Exists(todosFilePath))
            {
                return new List<StaffRecord>();
            }

            var json = File.ReadAllText(todosFilePath);

            return JsonSerializer.Deserialize<List<StaffRecord>>(json);
        }

        public static List<StaffRecord> Create(string StaffName, string ItemName, string ItemRetrievedDate, int Quantity)
        {

            List<StaffRecord> ItemDetails = GetAll();
            ItemDetails.Add(
                new StaffRecord
                {
                    StaffName = StaffName,
                    ItemName = ItemName,
                    ItemRetrievedDate = ItemRetrievedDate,
                    Quantity = Quantity
                }
                );
            SaveAll(ItemDetails);
            return ItemDetails;
        }

    }
}
