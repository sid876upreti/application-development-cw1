﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Wed2.Data
{
    public class InventoryService
    {
        public string message { get; set; }
        bool alreadyExists = false;


        private static void SaveAll(List<InventoryItems> inventory)
        {
            string appDataDirectoryPath = Utils.GetAppDirectoryPath();
            string inventoryFilePath = Utils.GetInventoryFilePath();

            if (!Directory.Exists(appDataDirectoryPath))
            {
                Directory.CreateDirectory(appDataDirectoryPath);
            }

            var json = JsonSerializer.Serialize(inventory);
            File.WriteAllText(inventoryFilePath, json);
        }

        public static List<InventoryItems> GetAll()
        {
            string todosFilePath = Utils.GetInventoryFilePath();
            if (!File.Exists(todosFilePath))
            {
                return new List<InventoryItems>();
            }

            var json = File.ReadAllText(todosFilePath);

            return JsonSerializer.Deserialize<List<InventoryItems>>(json);
        }

        public static List<InventoryItems> Create(string ItemName,int Quantity)
        {
            
            List<InventoryItems> ItemDetails = GetAll();
            ItemDetails.Add(
                new InventoryItems
                {
                    ItemName= ItemName,
                    Quantity=Quantity
                }
                );
            SaveAll(ItemDetails);
            return ItemDetails;


        }

       public static List<InventoryItems> Update(string ItemName,int quantity)
        {
            List<InventoryItems> ItemDetails = GetAll();
            InventoryItems inventoryItemsUpdate = ItemDetails.FirstOrDefault(x=>x.ItemName == ItemName);
            inventoryItemsUpdate.Quantity = inventoryItemsUpdate.Quantity + quantity;
            SaveAll(ItemDetails);
            return ItemDetails;
        }

        public static List<InventoryItems> Remove_Item(string ItemName, int quantity, string LastTakenOut)
        {
            List<InventoryItems> ItemDetails = GetAll();
            InventoryItems inventoryItemsUpdate = ItemDetails.FirstOrDefault(x => x.ItemName == ItemName);
            inventoryItemsUpdate.Quantity = inventoryItemsUpdate.Quantity - quantity;
            inventoryItemsUpdate.LastTakenOut = LastTakenOut;
            SaveAll(ItemDetails);
            return ItemDetails;
        }


        //public static List<User> Delete(Guid id)
        //{
        //    List<User> users = GetAll();
        //      User user = users.FirstOrDefault(x => x.Id == id);

        //    if (user == null)
        //   {
        //      throw new Exception("User not found.");
        //   }

        //  TodosService.DeleteByUserId(id);
        //  users.Remove(user);
        //  SaveAll(users);

        //   return users;
        //}
    }
}
