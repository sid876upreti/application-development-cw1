﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wed2.Data
{
    public class StaffRecord
    {
        public Guid Id { get; set; }
        public string StaffName { get; set; }
        public string ItemName { get ; set; }
        public string ItemRetrievedDate { get; set; }
        public int Quantity { get; set; }
    }
}
