﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;


namespace Wed2.Data
{
    public static class UsersService
    {
        public const string SeedUsername = "admin";
        public const string SeedPassword = "admin";
        public const string SeedPhoneNumber = "1234567890";

        private static void SaveAll(List<User> users)
        {
            string appDataDirectoryPath = Utils.GetAppDirectoryPath();
            string appUsersFilePath = Utils.GetAppUsersFilePath();

            if (!Directory.Exists(appDataDirectoryPath))
            {
                Directory.CreateDirectory(appDataDirectoryPath);
            }

            var json = JsonSerializer.Serialize(users);
            File.WriteAllText(appUsersFilePath, json);
        }

        public static List<User> GetAll()
        {
            string appUsersFilePath = Utils.GetAppUsersFilePath();
            if (!File.Exists(appUsersFilePath))
            {
                return new List<User>();
            }

            var json = File.ReadAllText(appUsersFilePath);

            return JsonSerializer.Deserialize<List<User>>(json);
        }

        public static List<User> Create(Guid userId, string Email, string password, string PhoneNumber, Role role)
        {
            List<User> users = GetAll();
            bool usernameExists = users.Any(x => x.Email ==  Email);

            if (usernameExists)
            {
                throw new Exception("Username already exists.");
            }

            users.Add(
                new User
                {
                    Email = Email,
                    PasswordHash = Utils.HashSecret(password),
                    Role = role,
                    PhoneNumber = PhoneNumber,
                    CreatedBy = userId
                }
            );
            SaveAll(users);
            return users;
        }

        public static void SeedUsers()
        {
            var users = GetAll().FirstOrDefault(x => x.Role == Role.Admin);

            if (users == null)
            {
                Create(Guid.Empty, SeedUsername, SeedPassword,SeedPhoneNumber, Role.Admin);
            }
        }

        public static User GetById(Guid id)
        {
            List<User> users = GetAll();
            return users.FirstOrDefault(x => x.Id == id);
        }

        //public static List<User> Delete(Guid id)
        //{
        //    List<User> users = GetAll();
        //    User user = users.FirstOrDefault(x => x.Id == id);

        //    if (user == null)
        //    {
        //        throw new Exception("User not found.");
        //    }

        //    TodosService.DeleteByUserId(id);
        //    users.Remove(user);
        //    SaveAll(users);

        //    return users;
        //}

        public static User Login(string Email, string password)
        {
            var loginErrorMessage = "Invalid username or password.";
            List<User> users = GetAll();
            User user = users.FirstOrDefault(x => x.Email == Email);

            if (user == null)
            {
                throw new Exception(loginErrorMessage);
            }

            bool passwordIsValid = Utils.VerifyHash(password, user.PasswordHash);

            if (!passwordIsValid)
            {
                throw new Exception(loginErrorMessage);
            }

            return user;
        }

        public static User ChangePassword(Guid id, string currentPassword, string newPassword)
        {
            if (currentPassword == newPassword)
            {
                throw new Exception("New password must be different from current password.");
            }

            List<User> users = GetAll();
            User user = users.FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                throw new Exception("User not found.");
            }

            bool passwordIsValid = Utils.VerifyHash(currentPassword, user.PasswordHash);

            if (!passwordIsValid)
            {
                throw new Exception("Incorrect current password.");
            }

            user.PasswordHash = Utils.HashSecret(newPassword);
            user.HasInitialPassword = false;
            SaveAll(users);

            return user;
        }
    }
}
