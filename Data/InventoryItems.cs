﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wed2.Data
{
    public class InventoryItems
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string ItemName { get; set; } 
        public int Quantity { get;set; }
        public string LastTakenOut { get; set; }
    }
}
